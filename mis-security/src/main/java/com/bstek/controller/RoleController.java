package com.bstek.controller;

import com.bstek.model.Resource;
import com.bstek.model.RoleDto;
import com.bstek.service.ResourceService;
import com.bstek.service.RoleService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RoleController  {

    @Autowired
    RoleService roleService;

    @Autowired
    ResourceService resourceService;


    @GetMapping(value = "/loadRoleList")
    public PageInfo<RoleDto> loadRoleList(@RequestParam Map<String, Object> parameter){
        return roleService.loadRoleList(parameter);
    }

    @GetMapping(value = "/getResourceByRoleId")
    public List<Resource> getResourceByRoleId(@RequestParam String roleId){
        List<Resource> perRes = new ArrayList<>();
        List<Resource> allRes = resourceService.findAllWithUrl();
         perRes = roleService.getResourceByRoleId(roleId);
         Map perMap = new HashMap<String, Object>();
         perRes.forEach(res->{
             perMap.put(res.getId(),res);
         });
         if(!perRes.isEmpty()){
             allRes.forEach(item->{
                 Resource r = (Resource) perMap.get(item.getId());
                 if(r!=null){
                     item.setChecked(true);
                 }
             });
         }
        return allRes;
    }
}
