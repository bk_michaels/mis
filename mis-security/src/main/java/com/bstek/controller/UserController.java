package com.bstek.controller;


import com.bstek.model.UserDto;
import com.bstek.service.UserService;
import com.bstek.utils.SecurityUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/currentUser")
    public UserDto user(Principal user){
        return SecurityUtils.getCurrentUserUser();
    }

    @GetMapping("/getUserByUsername")
    public UserDto getUserByUsername(@RequestParam (value = "username") String username){
        return userService.getUserByUsername(username);
    }

    @GetMapping(value = "/loadUserList")
    public PageInfo<UserDto> loadUser(@RequestParam Map<String, Object> parameter){
       return userService.loadUser(parameter);
    }


}
