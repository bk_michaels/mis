package com.bstek.controller;

import com.bstek.model.UserDto;
import com.bstek.service.RoleMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoleMemberController {


    @Autowired
    RoleMemberService roleMemberService;

    @GetMapping(value = "/loadUserDtoByRoleIdList")
    public List<UserDto> loadUserDtoByRoleIdList(String roleId) {
        return roleMemberService.loadUserDtoByRoleIdList(roleId);
    }
}
