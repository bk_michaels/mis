package com.bstek.controller;

import com.bstek.model.Resource;
import com.bstek.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MenuController {

    @Autowired
    ResourceService resourceService;

    @GetMapping(value = "/menus")
    public List<Resource> getRouter(String username){
        return resourceService.findAllWithUrl();
    }


    @GetMapping(value = "/getResourceById")
    public Resource findResourceById(String id) {
        return resourceService.findResourceById(id);
    }
}
