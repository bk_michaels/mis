package com.bstek.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BController {

    @GetMapping("/bb/testb")
    @PreAuthorize("hasAuthority('oauth.bb.testB')")
    public String test(){
        return "这是BB角色的权限";
    }

}
