package com.bstek.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AController {


    @GetMapping("/aa/testA")
    @PreAuthorize("hasAuthority('oauth.aa.testA')")
    public String test(){
        return "这是AA角色的权限";
    }
    @GetMapping(value = "aa/hello1")
    public String testUaa(){
        return "this is aa hello1";
    }


}
