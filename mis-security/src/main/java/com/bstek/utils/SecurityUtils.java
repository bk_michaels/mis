package com.bstek.utils;

import com.bstek.model.UserDto;
import com.bstek.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtils {

    public static UserDto getCurrentUserUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDto) {
            return (UserDto) authentication.getPrincipal();
        }else{
            UserService userService = (UserService) ContextHolder.getBean(UserService.BEAN_ID);
            UserDto user = new UserDto();
            user.setUsername(authentication.getName());
            return userService.getUserByUsername(authentication.getName());
        }
    }
}
