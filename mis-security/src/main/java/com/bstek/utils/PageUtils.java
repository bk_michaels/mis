package com.bstek.utils;

import java.io.Serializable;
import java.util.List;

/**
 * @since 2018年7月23日
 * @author michael
 */
public class PageUtils implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6288683430899407732L;

	private int total;
	
	private List<?> rows;
	
	
	public PageUtils(int total, List<?> rows) {
		this.total = total;
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}
	
	
	
	
	
}
