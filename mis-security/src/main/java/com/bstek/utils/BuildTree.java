package com.bstek.utils;

import com.bstek.model.Resource;

import java.util.ArrayList;
import java.util.List;

public class BuildTree {

    public static List<Resource> build(List<Resource> nodes) {
        if (nodes == null) {
            return null;
        }
        List<Resource> topNodes = new ArrayList<Resource>();
        for (Resource child : nodes) {
            String pid = child.getParentId();
            if (pid == null) {
                topNodes.add(child);
                continue;
            }
            for (Resource parent : nodes) {
                String id = parent.getId();
                if (id != null && id.equals(pid)) {
                    parent.getChildren().add(child);
                    continue;
                }
            }
        }
        return topNodes;
    }



}
