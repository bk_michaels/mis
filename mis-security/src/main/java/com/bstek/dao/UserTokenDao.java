package com.bstek.dao;

import com.bstek.model.UserToken;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserTokenDao {

    void insertToken(UserToken token);

    List<UserToken> getTokenByUserId(String userId);

    void updateToken(UserToken token);
}
