package com.bstek.dao;


import com.bstek.model.UserDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserDao {

   UserDto getUserByUsername(String username);


   List<UserDto> loadUser(Map<String,Object> parameter);



}
