package com.bstek.dao;

import com.bstek.model.RoleUserDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleUserDao {

    List<RoleUserDto>  getRoleUserByUsername(String username);
}
