package com.bstek.dao;

import com.bstek.model.UserDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMemberDao {


    List<UserDto> loadUserDtoByRoleIdList(String roleId);

}
