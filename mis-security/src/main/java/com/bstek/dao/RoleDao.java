package com.bstek.dao;


import com.bstek.model.Resource;
import com.bstek.model.RoleDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoleDao {

     RoleDto getRoleByUsername();

     List<RoleDto> loadRoleList(Map<String, Object> parameter);

     List<Resource> getResourceByRoleId(String roleId);

}
