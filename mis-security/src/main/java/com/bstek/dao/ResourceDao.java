package com.bstek.dao;


import com.bstek.model.Resource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ResourceDao {

    List<Resource> findAllWithUrl();

    List<Resource> findResourceById(String id);
}
