package com.bstek.dao;

import com.bstek.model.PermissionDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PermissionDao {

    List<PermissionDto> searchPermissionByRoleId(String roleId);
}
