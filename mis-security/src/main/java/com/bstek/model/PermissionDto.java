package com.bstek.model;

public class PermissionDto {

    //权限id
    private String id;

    //角色id
    private String roleId;

    /**
     * 权限鉴别属性
     * @return attribute
     */
    private String attribute;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getAttribute() {
        return attribute;
    }
}
