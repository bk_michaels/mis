package com.bstek.service;

import com.bstek.model.Resource;
import com.bstek.model.RoleDto;
import com.github.pagehelper.PageInfo;

import java.util.List;
import java.util.Map;

public interface RoleService {


    PageInfo<RoleDto> loadRoleList(Map<String, Object> parameter);

    List<Resource> getResourceByRoleId(String roleId);
}
