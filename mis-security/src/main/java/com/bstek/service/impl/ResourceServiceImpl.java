package com.bstek.service.impl;


import com.bstek.dao.PermissionDao;
import com.bstek.dao.ResourceDao;
import com.bstek.model.Resource;
import com.bstek.service.ResourceService;
import com.bstek.utils.BuildTree;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    ResourceDao resourceDao;

    @Autowired
    PermissionDao permissionDao;

    @Override
    public List<Resource> findAllWithUrl() {
        List<Resource> res =  resourceDao.findAllWithUrl();
        return BuildTree.build(res);
    }


    @Override
    public Resource findResourceById(String id) {
        return resourceDao.findResourceById(id).get(0);
    }



}
