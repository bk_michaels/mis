package com.bstek.service.impl;

import com.bstek.dao.RoleDao;
import com.bstek.model.Resource;
import com.bstek.model.RoleDto;
import com.bstek.service.RoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleDao roleDao;

    @Override
    public PageInfo<RoleDto> loadRoleList(Map<String, Object> parameter) {
        String pageNum = (String) parameter.get("pageNum");
        String pageSize = (String) parameter.get("pageSize");
        PageHelper.startPage(Integer.valueOf(pageNum),Integer.valueOf(pageSize));
        List<RoleDto> res = roleDao.loadRoleList(parameter);
        PageInfo<RoleDto> pageInfo = new PageInfo<RoleDto>(res);
        return pageInfo;
    }

    @Override
    public List<Resource> getResourceByRoleId(String roleId) {
        return null;
    }
}
