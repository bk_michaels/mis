package com.bstek.service.impl;


import com.bstek.dao.UserDao;
import com.bstek.model.UserDto;
import com.bstek.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service(UserService.BEAN_ID)
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDto getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

    @Override
    public PageInfo<UserDto> loadUser(Map<String,Object> parameter){
        String pageNum = (String) parameter.get("pageNum");
        String pageSize = (String) parameter.get("pageSize");
        PageHelper.startPage(Integer.valueOf(pageNum),Integer.valueOf(pageSize));
        List<UserDto> res =  userDao.loadUser(parameter);
        PageInfo<UserDto> pageInfo = new PageInfo<UserDto>(res);
       return pageInfo;
    }
}
