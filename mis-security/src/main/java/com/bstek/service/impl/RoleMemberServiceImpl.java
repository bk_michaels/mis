package com.bstek.service.impl;

import com.bstek.dao.RoleMemberDao;
import com.bstek.model.UserDto;
import com.bstek.service.RoleMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleMemberServiceImpl implements RoleMemberService {

    @Autowired
    RoleMemberDao roleMemberDao;

    @Override
    public List<UserDto> loadUserDtoByRoleIdList(String roleId) {
        return roleMemberDao.loadUserDtoByRoleIdList(roleId);
    }
}
