package com.bstek.service;


import com.bstek.model.UserDto;
import com.github.pagehelper.PageInfo;

import java.util.Map;

public interface UserService {

    public static final String BEAN_ID = "sec.userService";

    UserDto getUserByUsername(String username);

    PageInfo<UserDto> loadUser(Map<String,Object> parameter);
}
