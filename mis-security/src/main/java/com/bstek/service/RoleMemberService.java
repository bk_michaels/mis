package com.bstek.service;

import com.bstek.model.UserDto;

import java.util.List;

public interface RoleMemberService {

    List<UserDto> loadUserDtoByRoleIdList(String roleId);

}
