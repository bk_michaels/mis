package com.bstek.service;




import com.bstek.model.Resource;

import java.util.List;

public interface ResourceService {

    List<Resource> findAllWithUrl();


    Resource findResourceById(String id);

}
