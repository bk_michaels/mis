package com.bstek;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = {"com.bstek.dao"})
@SpringBootApplication
public class MisSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(MisSecurityApplication.class, args);
    }

}
