package com.bstek.communication.model;


/**
 * 通讯录管理
 * @author Caoguojin
 *
 */
public class Communication {

	/**
	 * 账号
	 */
	private String username;
	
	/**
	 * 姓名
	 */
	private String cname;
	
	/**
	 * 别名
	 */
	private String ename;
	/**
	 * 性别
	 */
	private String sex;
	
	/**
	 * 手机
	 */
	private String phone;
	
	/**
	 * 座机
	 */
	private String tel;
	
	/**
	 * 邮箱
	 */
	private String email;
	
	/**
	 * 地址
	 */
	private String address;
	
	/**
	 * 是否激活
	 */
	private boolean isActivation;
	
	/**
	 * 是否禁用
	 */
	private boolean isDisable;
	
	/**
	 * 是否关注微工作台
	 */
	private boolean focusWorkbench;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isActivation() {
		return isActivation;
	}

	public void setActivation(boolean isActivation) {
		this.isActivation = isActivation;
	}

	public boolean isDisable() {
		return isDisable;
	}

	public void setDisable(boolean isDisable) {
		this.isDisable = isDisable;
	}

	public boolean isFocusWorkbench() {
		return focusWorkbench;
	}

	public void setFocusWorkbench(boolean focusWorkbench) {
		this.focusWorkbench = focusWorkbench;
	}
	
}
