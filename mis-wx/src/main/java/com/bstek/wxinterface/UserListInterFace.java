package com.bstek.wxinterface;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.bstek.utils.ConstantlUtil;
import com.bstek.utils.WeixinCommUtil;

/**
 * 企业微信 用户通讯录接口 操作
 * @author snail
 * @date 2019年1月2日
 */
public class UserListInterFace {

	private static final Logger log=LoggerFactory.getLogger(UserListInterFace.class);
	
	/**
	 * 通过用户名 获取通信录下的用户信息
	 * @author snail
	 * @date 2018年12月12日
	 * @throws IOException
	 */
	public static JSONObject getUsers(String userID) throws IOException {
		if(StringUtils.hasText(userID)) {
			String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.corpSecret); //获取ACCESS_TOKEN
			String url=ConstantlUtil.getUserURL; //获取用户的url
			url=url.replace("ACCESS_TOKEN", accessToken).replace("USERID", userID); //组装替换url参数
			JSONObject resultJson =WeixinCommUtil.getWeixInfo(url,"GET"); //调用接口 返回json
			if(resultJson!=null) {
				log.info("通过用户名获取用户信息:"+resultJson.toJSONString());
				return resultJson;
			}
		}
		return null;
		
	} 
	
	/**
	 * 获取部门信息(部门ID不填 默认返回所有部门)
	 * @author snail
	 * @date 2018年12月12日
	 * @throws IOException
	 */
	public static JSONObject getDemparts(String deptId) throws IOException {
		String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.corpSecret);
		String url=ConstantlUtil.getDemptURL;
		url=url.replace("ACCESS_TOKEN", accessToken).replace("ID", deptId);
		JSONObject resultJson =WeixinCommUtil.getWeixInfo(url,"GET");
		if(resultJson!=null) {
			log.info("获取部门信息:"+resultJson.toJSONString());
			return resultJson;
		}
		return null;
	} 
	
	/**
	 * 获取部门下用户信息
	 * @author snail
	 * @date 2018年12月12日
	 * @throws IOException
	 */
	public static JSONObject getDempartUsers(String deptId) throws IOException {
		String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.corpSecret);
		String url=ConstantlUtil.getDemptUserURL;
		url=url.replace("ACCESS_TOKEN", accessToken).replace("DEPARTMENT_ID", deptId).replace("FETCH_CHILD", "0");
		JSONObject resultJson =WeixinCommUtil.getWeixInfo(url,"GET");
		if(resultJson!=null) {
			log.info("获取部门下用户信息:"+resultJson.toJSONString());
			return resultJson;
		}
		return null;
	} 
	
	/**
	 * 创建用户信息
	 * @author snail
	 * @date 2018年12月12日
	 * @throws IOException
	 */
	public static JSONObject createUser(String content) throws IOException {
		String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.corpSecret);
		String url=ConstantlUtil.createUserURL;
		url=url.replace("ACCESS_TOKEN", accessToken);
		//String content=BodyUtil.createUser();
		JSONObject resultJson =WeixinCommUtil.operatorWeixin(url, "POST", content);
		if(resultJson!=null) {
			log.info("返回创建用户结果:"+resultJson.toJSONString());
			return resultJson;
		}
		return null;
	} 
	
	/**
	 * 修改用户信息
	 * @author snail
	 * @date 2018年12月12日
	 * @throws IOException
	 */
	public static JSONObject updateUser(String content) throws IOException {
		String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.corpSecret);
		String url=ConstantlUtil.udpateUserURL;
		url=url.replace("ACCESS_TOKEN", accessToken);
		JSONObject resultJson =WeixinCommUtil.operatorWeixin(url, "POST", content);
		if(resultJson!=null) {
			log.info("返回更新用户结果:"+resultJson.toJSONString());
			return resultJson;
		}
		return null;
	} 
	
	/**
	 * 删除用户信息
	 * @author snail
	 * @date 2018年12月12日
	 * @throws IOException
	 */
	public static JSONObject deleteUser(String userId) throws IOException {
		String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.corpSecret);
		String url=ConstantlUtil.deleteUserURL;
		url=url.replace("ACCESS_TOKEN", accessToken).replace("USERID", userId);
		JSONObject resultJson =WeixinCommUtil.getWeixInfo(url, "GET");
		if(resultJson!=null) {
			log.info("返回删除用户结果:"+resultJson.toJSONString());
			return resultJson;
		}
		return null;
	} 
	
	
	public static void main(String[] args) throws IOException {
		UserListInterFace.getUsers("Baiguoyu");
	}
}
