package com.bstek.wxinterface;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.bstek.utils.BodyUtil;
import com.bstek.utils.ConstantlUtil;
import com.bstek.utils.WeixinCommUtil;

/**
 * 应用 打卡信息
 * @author snail
 * @date 2019年1月2日
 */
public class CheckInInterFace {
	
	public static final Logger log=LoggerFactory.getLogger(CheckInInterFace.class);
	/**
	 *    获取 打卡信息
	 * @author snail
	 * @throws IOException 
	 * @date 2018年12月15日
	 */
	public static JSONObject getCheckinData(String checkinBody) throws IOException {
		//获取打卡信息url
		if(StringUtils.hasText(checkinBody)) {
			String checkinURL=ConstantlUtil.getCheckinDataURL;
			String accessToken=WeixinCommUtil.getWeixinToken(ConstantlUtil.corpId, ConstantlUtil.checkSecret);
			checkinURL=checkinURL.replace("ACCESS_TOKEN", accessToken);
			JSONObject json=WeixinCommUtil.operatorWeixin(checkinURL, "POST", checkinBody);
			log.info("打卡信息"+json);
			return json;
		}else {
			log.info("查询打卡信息参数为空"+checkinBody);
			return null;
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		String checkinBody=BodyUtil.checkinBody();
		getCheckinData(checkinBody);
	}
}
