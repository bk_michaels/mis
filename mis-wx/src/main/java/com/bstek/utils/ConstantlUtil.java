package com.bstek.utils;

/**
 * 公共参数维护
 * @author snail
 * @date 2019年1月2日
 */
public class ConstantlUtil {
	//企业ID
	public final static String corpId="wwc31079c083f352b7";
	//通信的的凭证密钥
	public final static String corpSecret="cpMpDZc1GpWWH6CovyJN5kDMSiGjP7bFxYapE1gr-mA";
	
	//获微信 调用接口凭证  access_token  corpid:企业ID corpsecret:没个应用的机密参数
	public static String getTokenURL="https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=CORPID&corpsecret=CORPSECRET";
	//获取用户信息 access_token 调用接口凭证  userid 用户id(用户名)
	public static String getUserURL="https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID";
	//获取部门 access_token id 部门id。获取指定部门及其下的子部门。 如果不填，默认获取全量组织架构
	public static String getDemptURL="https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=ID";
	// 获取 部门下的成员   department_id 部门ID fetch_child(1/0)：是否递归获取子部门下面的成员
	public static String getDemptUserURL="https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN"+
		"&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD";
	//获取部门成员详情(信息量比较多) department_id 部门ID fetch_child(1/0)：是否递归获取子部门下面的成员
	public static String getDemptUserDetailURL="https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN"+ 
		"&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD";
	//创建用户 地址
	public static String createUserURL="https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=ACCESS_TOKEN";
	//修改用户 地址
	public static String udpateUserURL="https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN";
	//修改用户 地址
	public static String deleteUserURL="https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=ACCESS_TOKEN&userid=USERID";
	
	
	//通讯录推送参数
	public final static String token = "Hcu4O6ivBtFn7b";//token
	// encodingAESKey
	public final static String encodingAESKey = "kTidP8EKTDJONlUcnLaYdyDeqPUxCC5uVfkTiBiHKcB";
	
	//打卡的凭证密钥
	public final static String checkSecret="wnvH7HQc2eKKl8XPHi07DmYjDGtQu4MlcIWpyn9vN9g";
	//打卡数据接口
	public final static String getCheckinDataURL="https://qyapi.weixin.qq.com/cgi-bin/checkin/getcheckindata?access_token=ACCESS_TOKEN";
	//审批的凭证密钥
	public final static String approvalSecret="V6L3YL7hLIT0ioG7CMDK9AOo3yuf6A-Gyv3poPufMzI";
	//审批数据接口
	public final static String getapprovalDataURL="https://qyapi.weixin.qq.com/cgi-bin/corp/getapprovaldata?access_token=ACCESS_TOKEN";
	
	
}
