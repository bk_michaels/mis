package com.bstek.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

/**
 * 时间工具类
 * @author snail
 * @date 2019年1月2日
 */
public class DateUtil {

	/**
	 * 时间格式化
	 * @author snail
	 * @date 2018年12月15日
	 * @param format
	 * @param dataStr
	 * @return
	 * @throws ParseException 
	 */
	public static Date getDate(String format,String dataStr) throws ParseException {
		if(StringUtils.hasText(format)&&StringUtils.hasText(dataStr)) {
			SimpleDateFormat sdf=new SimpleDateFormat(format);
			Date testDate=sdf.parse(dataStr);
			return testDate;
		}
		return null;
	}
	
	
}
