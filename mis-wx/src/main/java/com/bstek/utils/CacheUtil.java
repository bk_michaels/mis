package com.bstek.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 缓存 参数信息
 * @author snail
 * @date 2019年1月2日
 */
public class CacheUtil {
	
	private static Map<String, Object> cache=new HashMap<>();
	
	/**
	 * 存放缓存内容
	 * @author snail
	 * @date 2019年1月1日
	 * @param key
	 * @param value
	 * @return
	 */
	public static synchronized void setCacheParams(String key,Object value) {
		
		cache.put(key, value);
	}
	
	/**
	 * 获取缓存内容
	 * @author snail
	 * @date 2019年1月1日
	 * @param key
	 * @return
	 */
	public static Object getCacheParams(String key) {
		
		return cache.get(key);
	}
	
	/**
	 * 删除缓存内容
	 * @author snail
	 * @date 2019年1月1日
	 * @param key
	 * @return
	 */
	public static synchronized Object delCacheParams(String key) {
		
		return cache.remove(key);
	}
	
}
