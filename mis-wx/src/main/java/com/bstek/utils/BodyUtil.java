package com.bstek.utils;

import java.text.ParseException;
import java.util.Date;
/**
 * 组装发送 微信接口请求内容
 * @author snail
 * @date 2019年1月2日
 */
public class BodyUtil {
	
	public static String createUser() {
		String userInfo="{" + 
				"\"userid\": \"zhangsan\"," + 
				"\"name\": \"测试02(改)\"," + 
				"\"alias\": \"jackzhang\"," + 
				"\"mobile\": \"15913215421\"," + 
				"\"department\": [1]," + 
				"\"order\":[0]," + 
				"\"position\": \"测试经理\"," + 
				"\"gender\": \"2\"," + 
				"\"email\": \"ceshi001@gzdev.com\"," + 
				"\"isleader\": 1," + 
				"\"enable\":1," + 
				"\"avatar_mediaid\": \"\"," + 
				"\"telephone\": \"020-123456\"," + 
				"\"extattr\": {\"attrs\":[{\"name\":\"爱好\",\"value\":\"旅游\"},{\"name\":\"卡号\",\"value\":\"1234567234\"}]}," + 
				"\"to_invite\": false," + 
				"\"external_position\": \"高级产品经理\"," + 
				"\"external_profile\": {" + 
				"\"external_attr\": [" + 
				"{" + 
				"\"type\": 0,"+ 
				"\"name\": \"文本名称\"," + 
				"\"text\": {" + 
				"\"value\": \"文本\"" + 
				"}" + 
				"}," + 
				"{" + 
				"\"type\": 1," + 
				"\"name\": \"网页名称\"," + 
				"\"web\": {" + 
				"\"url\": \"http://www.test.com\"," + 
				"\"title\": \"标题\"" + 
				"}" + 
				"}," + 
				"{" + 
				"\"type\": 2," + 
				"\"name\": \"测试app\"," + 
				"\"miniprogram\": {" + 
				"\"appid\": \"wx8bd80126147df384\"," + 
				"\"pagepath\": \"/index\"," + 
				"\"title\": \"mytest\"" + 
				"}" + 
				"}" + 
				"]" + 
				"}" + 
				"}";
		return userInfo;
	}
	
	public static String checkinBody() throws ParseException {
		Date startDate=DateUtil.getDate("yyyyMMdd HH:mm:ss", "20181214 00:00:00");
		Date endDate=DateUtil.getDate("yyyyMMdd HH:mm:ss", "20181216 00:00:00");
		String chenin="{"+ 
				"\"opencheckindatatype\": 3,"+
				"\"starttime\": "+startDate.getTime()+","+
				"\"endtime\": "+endDate.getTime()+","+
				"\"useridlist\": [\"BaiGuoYu\",\"bgy123456\"]"+
				"}";
		return chenin;
	}
	
	public static String approvalBody() throws ParseException{
		Date startDate=DateUtil.getDate("yyyyMMdd HH:mm:ss", "20181214 00:00:00");
		Date endDate=DateUtil.getDate("yyyyMMdd HH:mm:ss", "20181216 00:00:00");
		String approval="{"+ 
				"\"starttime\": "+startDate.getTime()+","+
				"\"endtime\": "+endDate.getTime()+","+
				"\"next_spnum\": "+
				"}";
		return approval;
	}
	
	
	public static void main(String[] args) throws ParseException {
		//System.out.println(BodyUtil.createUser());
		System.out.println(checkinBody());
	}
	
}
