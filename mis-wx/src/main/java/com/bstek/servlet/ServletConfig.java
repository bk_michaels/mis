package com.bstek.servlet;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * servlet配置类 spring boot启动会自动加载
 * @author snail
 * @date 2019年1月2日
 */
@Configuration
public class ServletConfig {
	
	/**
	 * servlet注册 启动不需要(@ServletComponentScan)注解
	 * @author snail
	 * @date 2018年12月17日
	 * @return
	 */
	@Bean
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServletRegistrationBean servletBean() {
		return new ServletRegistrationBean(
				new WeixinServlet(), "/bstek/weixinServlet");
	}
}
