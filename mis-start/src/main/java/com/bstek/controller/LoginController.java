package com.bstek.controller;

import com.bstek.model.LoginDTO;
import com.bstek.model.UserToken;
import com.bstek.model.dto.Result;
import com.bstek.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;


@Controller
public class LoginController {

	public Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	BaseUrlService baseUrlService;

	@Autowired
	BaseUserService userService;

	@RequestMapping(value="/login",method=RequestMethod.GET)
	public String login(){
		return "login";
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.GET)
	public String logout(){
		return "redirect:/login";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	@ResponseBody
	public Result login(@RequestBody LoginDTO loginDto) {
		String username = loginDto.getUsername();
		String pwd = loginDto.getPwd();
	    BaseUser  user = userService.findUserByUsername(username);
	    if(user==null){
			return Result.error("用户或密码错误");
		}
		UserToken userToken = new UserToken(user.getUsername(), user.getNickname());
		String token="";
		try {
			token = JwtUtils.generateToken(userToken, 2*60*60*1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Result r = new Result();
		return r.ok("登录成功")
				.put("token", token)
				.put("user",user)
				.put("menus",baseUrlService.findAllUrls());
	}

	@RequestMapping(value="/logout",method=RequestMethod.GET)
	Result logout(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("用户退出.....");
		return Result.ok();
	}

	private List<BaseUrl> getBaseUrl(){
		List<BaseUrl> result = new ArrayList<>();
		BaseUrl url1 = new BaseUrl();
		url1.setId("1");
		url1.setName("系统设置");
		url1.setPath(null);
		url1.setParentId(null);

		BaseUrl url2 = new BaseUrl();
		url2.setId("2");
		url2.setName("用户维护");
		url2.setPath("/user");
		url2.setParentId("1");


		BaseUrl url3 = new BaseUrl();
		url3.setId("3");
		url3.setName("角色维护");
		url3.setPath("/role");
		url3.setParentId("1");

		BaseUrl url4 = new BaseUrl();
		url4.setId("4");
		url4.setName("导航二");
		url4.setPath("/a");
		url4.setParentId(null);

		BaseUrl url5 = new BaseUrl();
		url5.setId("5");
		url5.setName("导航三");
		url5.setPath("/b");
		url5.setParentId(null);

		BaseUrl url6 = new BaseUrl();
		url6.setId("6");
		url6.setPath("/c");
		url6.setName("导航四");
		url6.setParentId(null);

		url1.getChildren().add(url2);
		url1.getChildren().add(url3);

		result.add(url1);
		result.add(url4);
		result.add(url5);
		result.add(url6);
		return result;
	}


}
