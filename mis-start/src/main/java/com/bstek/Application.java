package com.bstek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication(scanBasePackages = {"com.bstek"})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	@Bean
	public FilterRegistrationBean getContextFilter(){
		FilterRegistrationBean registrationBean=new FilterRegistrationBean();
		registrationBean.setFilter(contextFilter());
		registrationBean.setEnabled(true);
		List<String> urlPatterns=new ArrayList<String>();
		urlPatterns.add("/*");//拦截路径，可以添加多个
		registrationBean.setUrlPatterns(urlPatterns);
		registrationBean.setName("contextfilter");
		registrationBean.setOrder(1);
		return registrationBean;
	}
	@Bean
	public ContextFilter contextFilter(){
		return new ContextFilter();
	}

}
