package com.bstek.model;

import java.io.Serializable;

/**
 * @author michael
 * @version V1.0
 */
public class UserToken implements Serializable{
    private static final long serialVersionUID = 1L;

    public UserToken(String username,  String nickname) {
        this.username = username;
        this.nickname = nickname;
    }


    /**
     * 用户登录名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickname;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @Override
    public String toString() {
        return "UserToken{" +
                "username='" + username + '\'' +
                ", nickname='" + nickname + '\'' +
                '}';
    }
}
