package com.bstek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class MisZuulApplication {

	public static void main(String[] args) {
		SpringApplication.run(MisZuulApplication.class, args);
	}
}
